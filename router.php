<?php

/**
 * Gyural > Router
 *
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 * @version 1.10
 */

$router = LoadClass('router', 1);

\Gyu\Hooks::get('gyu.router.prepend', $router);

$router->map('is_gyural', function() {
	echo 'yes it is.';
	echo 'v. '. version . '<br /><br />';
	echo '<a href="http://www.mandarinoadv.com">http://www.mandarinoadv.com</a><br />';
	echo '<a href="http://www.gyural.com">http://www.gyural.com</a>';
});

# https://encelado.ocholding.com/avatar/300/1482774882-avatar.png
# https://encelado.ocholding.com/brand/1200/1500567835-DSC03565.JPG
$router->map('avatar/#/#', 'encelado/avatar/width:#/file:#');
$router->map('car/#', 'encelado/car/width:1400/file:#');
$router->map('postcard/#/#', 'encelado/postcard/width:#/file:#');
$router->map('brand/#/imp/#', 'encelado/CarMark/width:#/file:imp/#');
$router->map('brand/#/#', 'encelado/CarMark/width:#/file:#');

\Gyu\Hooks::get('gyu.router.append', $router);