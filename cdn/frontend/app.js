/* Applications Mixin(s) */

var ScrumsMixin = {
	methods: {
		Api_Scrum_Get: function(scrum_id) {

			Store.commit('unset_scrum');
			this.$http.post(this._api('scrums.get'), { scrum_id: scrum_id }).then(function(data) {
				Store.commit('set_scrum', data.data);
			}, function(data) {
				alert('Open the console for debug.');
				console.error(data);
			})

		},
		Api_Scrum_Context: function(context) {

			this.$http.post(this._api('scrums.get'), { scrum_id: Store.state.Scrum.data.scrum.scrum_id, contexts: [ context ] }).then(function(data) {
				Store.commit('add_context_scrum', { context: context.toLowerCase(), scrum: data.data.scrum });
			});

		},
		Current_Actor: function(actor_id) {

			return _.find(this.$store.state.Scrum.data.scrum.gyu_meta.actors, { scrums_actor_id: actor_id });

			return actor_id;
		}
	}
}

/* localstorage/Login checker. 

var tv_user = localStorage.getItem('tv_user');
if(tv_user != null) {
	Store.commit('set_user', JSON.parse(tv_user));
}
*/

Vue.component('scrum-app', httpVueLoader('/cdn/frontend/components/scrums/app.vue'));

var routes = [
	//{ path: '/users/login', component: httpVueLoader('/app/tv/_v/assets/controllers/users-login.vue'), meta: { requiresAuth: false } },
	//{ path: '/users/logout', component: httpVueLoader('/app/tv/_v/assets/controllers/users-logout.vue'), meta: { requiresAuth: true } },
	{ path: '/index', component: httpVueLoader('/cdn/frontend/views/index.vue'), meta: { requiresAuth: false }}
]

var router = new VueRouter({
	routes // short for `routes: routes`
});


Vue.mixin({
	methods: {
		_api: function(method) {
			return Store.state.EndPoint + method;
		}
	},
	computed: {
		me: function() {
			if(Store.state.User.user.token != undefined)
				return Store.state.User.user;
			return false;
		}
	}
});
/*

Vue.http.interceptors.push(function(request, next) {
	
	/*
	if(Store.state.User.user.token)
		Vue.http.headers.common['Authorization'] = 'Bearer ' + Store.state.User.user.token;
	else
		Vue.http.headers.common['Authorization'] = '';
	* /
	next();

});

router.beforeEach(function(to, from, next) {
	
	var canGo = true;
	/*
	if(to.matched[0] != undefined && to.matched[0].meta != undefined) {
		if(to.matched[0].meta.requiresAuth) {
			if(Store.state.User.user.token == undefined) {
				canGo = false;
				next({ path: '/users/login', query: { redirect: to.fullPath } });
			}
		}
	}
	* /
	if(canGo == true)
		next();
});*/

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.


//sync(Store, router);

var app = new Vue({
	router,
	store: Store,
	created: function() {}
}).$mount('#app')

// Now the app has started!