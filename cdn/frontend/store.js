window.Store = new Vuex.Store({
	state: {
		EndPoint: 'https://manifesto.mndrn.cloud/api/',
	},
	modules: {
		Scrum: {
			state: {
				data: false
			},
			mutations: {
				set_scrum: function(state, content) {
					state.data = content;
				},
				unset_scrum: function(state) {
					state.data = false;
				},
				add_context_scrum: function(state, content) {
					Vue.set(state.data.scrum.gyu_meta, content.context, content.scrum.gyu_meta[content.context]);
				}
			}
		},
		User: {
			state: {
				user: {},
			},
			mutations: {
				set_user: function(state, content) {
					localStorage.setItem('user', JSON.stringify(content));
					state.user = content;
				},
				logout_user: function(state) {
					localStorage.removeItem('user');
					state.user = false;
				}
			},
			actions: {
				auth: function(context, user_data) {
					context.commit('set_user', user_data.user);
                },
                logout: function(context) {
                    context.commit('set_user', false);
                }
            }
        }
    }
});