<?php

/**
 * Gyural - Command Line Interface - Shell
 *
 * @version 0.1
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

// Check if the script is called from shell
if (php_sapi_name() != "cli")
    die('Can run from outside the CLI! :D' . "\n\n");

define('absolute', __DIR__ . DIRECTORY_SEPARATOR);
define('cli', true);

$argv = $_SERVER["argv"];
$_SERVER["PATH_INFO"] = $argv[1];

// Check if there's enought info to route an Action
if(count($argv) < 2)
	die('Type: `php cli.php gyu_sdk help` for further help.' . "\n\n");

include_once(__DIR__ . '/index.php');

/**
 * Core of the Shell.
 * @return false
 */
function cli_handler() {

	global $argv;

	$cli = new Gyu\Cli();
	$string = 'Gyural ' . version . ' - CLI';

	for($a = 0; $a < strlen($string); $a++) $aa .= '=';

	echo "\n";
	echo $cli->string(' ' . $aa . ' ', 'white', 'red') . "\n";
	echo $cli->string(' ' . $string . ' ', 'white', 'red') . "\n";
	echo $cli->string(' ' . $aa . ' ', 'white', 'red') . "\n\n";

	if(!isset($argv[2]))
		$argv[2] = 'run';
	
	$params = $argv;
	unset($params[0], $params[1], $params[2]);

	$controller = LoadApp($argv[1], 1);
	if($controller) {
		
		$controller->cli = $cli;
		$action = 'Cli' . $argv[2];
		
		echo $cli->string('Controller: ' . $argv[1], 'red', null) . "\n";
		echo $cli->string('Action: ' . $argv[2], 'red', null) . "\n\n";

		echo $cli->string('Time: ' . date('r'), 'green', null) . "\n-\n\n";

		call_user_func_array(array($controller, $action), $params);

	}

	echo "\n\n";
}

die();
// Kill everything, to prevent the debug, etc... :)