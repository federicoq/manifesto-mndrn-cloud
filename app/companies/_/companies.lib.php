<?php

class companies extends application_object {

	var $gyu_table = 'companies';
	var $gyu_id = 'company_id';

	function deleteExecute() {

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());

		return parent::putExecute();

	}

	function ContextSimple() {

		$this->meta_add('a', 1);

	}

}