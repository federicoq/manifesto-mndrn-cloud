<?php

class calendars_events extends application_object {

	var $gyu_table = 'calendars_events';
	var $gyu_id = 'calendars_event_id';

	function deleteExecute() {

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());
		return parent::putExecute();

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());
		return parent::hangExecute();

	}

}