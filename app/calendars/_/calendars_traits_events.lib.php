<?php

trait calendars_traits_events {

    function calendar_createEvent($data_start, $data_end = false, $title = false, $description = false, $users = false) {
    	
    	$calendar = LoadClass('calendars/events', 1);
    	$calendar->setAttr('date_start', date('Y-m-d H:i:s', strtotime($data_start)));
    	$calendar->setAttr('date_end', date('Y-m-d H:i:s', strtotime($data_end ?: $data_start)));

    	if($title) $calendar->setAttr('title', $title);

    	if($description) $calendar->setAttr('description', $description);

    	if(!$data_end)
    		$calendar->setAttr('full_day', 1);

    	$calendar->setAttr('uid', get_class($this).':'.$this->{$this->gyu_id});

    	if($calendar->hangExecute()) {

	    	// Dopo L'insert..
			foreach($users as $user) {

				$events_user = LoadClass('calendars/events/users', 1);
				$events_user->setAttr('user_id', $user);
				$events_user->setAttr('calendars_event_id', $calendar->calendars_event_id);
				$events_user->setAttr('accepted', 1);
				$events_user->hangExecute();

			}
    		
    		return $calendar;

		}


    }

    function calendar_gets_events() {

    	return LoadClass('calendars/events', 1)->filter(['uid', get_class($this) . ':' . $this->{$this->gyu_id}], ['delete_time', 0]);

    }

}