<?php

class projects extends application_object {

	use calendars_traits_events;

	var $gyu_id = 'project_id';
	var $gyu_table = 'projects';

	function deleteExecute() {

		$this->setAttr('active', 0);

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());

		return parent::putExecute();

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());

		return parent::hangExecute();

	}

	//

	function get_company() {

		if(!$this->meta_get('company'))
			$this->meta_add('company', LoadClass('companies', 1)->get($this->company_id));

		return $this->meta_get('company');

	}

	/* Contexts */
	function ContextSimple() {

		$this->get_company();
		
	}

	function ContextCalendarsEvents() {

		$this->meta_add('events', $this->calendar_gets_events());

	}

}