<?php

class projectsCtrl extends application_controller {

	# http://manifesto.mndrn.cloud/api/projects.gets
	function ApiGets() {

		$projects = LoadClass('projects', 1)->filter_array($args);

		if($projects)
			return $this->response($this->ForContext($projects, 'Simple'));

		return $this->error('No Projects Found.');

	}

}