<?php

class scrumsCtrl extends application_controller {

	function ApiGet($scrum_id = false, $contexts = false) {

		if(!$scrum_id) $scrum_id = $_REQUEST['scrum_id'];
		if(!$contexts) $contexts = $_REQUEST['contexts'];

		if(!$contexts) $contexts = ['Actors', 'Statuses', 'Roles', 'Stories', 'Tags', 'Epics'];

		$scrum = LoadClass('scrums', 1)->get($scrum_id, $contexts);

		if(!$scrum) return $this->error();

		return $this->_data($scrum, 'scrum')->_r();


	}

	# https://manifesto.mndrn.cloud/api/scrum.dev
	function ApiDev() {

		$scrum = LoadClass('scrums', 1)->get(1, ['Actors', 'Statuses', 'Roles', 'Stories', 'Tags', 'Epics']);

		$actors = $scrum->actors_array();

		// New Story:
		$story = $scrum->story_create($actors[rand(0, count($actors)-1)], $story_type = 'story', $user_id = 0);

		if($story) {
			$body = $story->body([
				'title' => \Faker\Lorem::sentence($wordCount = 4),
				'description' => \Faker\Lorem::paragraph($wordCount = 4),
				'user_id' => 1,
				'estimate' => rand(1,32)
			]);

			if($body)
				$body->approve();

		}

		$scrum = LoadClass('scrums', 1)->get(1, ['Actors', 'Statuses', 'Roles', 'Stories', 'Tags', 'Epics']);

		/*
		for($a = 0; $a < 1000; $a++) {

			$stories[] = [
				'gyu_actor' => ['Utente', 'Amministratore', 'Altro Gruppo'][rand(0,2)],
				'title' => \Faker\Lorem::sentence($wordCount = 4),
				'description' => \Faker\Lorem::paragraph($wordCount = 4)
			];

		}

		foreach($stories as $story)
			$ss[] = $scrum->story_create($story['gyu_actor'], $story['title'], $story['description'], 1, true);

		return $ss;
		*/
		return $this->_data($scrum, 'scrum')->_r();

	}

	# https://manifesto.mndrn.cloud/api/scrums.create
	function ApiCreate() {

		//return false;
		
		$scrum = [
			'name' => 'OCH',
			'project_id' => 1,
			'gyu_roles' => [
				'Product Owner',
				'Scrum Master',
				'Developer',
				'Designer'
			],
			'gyu_statuses' => [
				'To Do',
				'In Progress',
				'Done'
			],
			'gyu_actors' => [
				'Utente Base',
				'Venditore',
				'Back Office',
				'Dpt Economico',
				'BDC',
				'Admin',
				'Api'
			],
			'gyu_epics' => [
				'Generale',
				'Veicoli',
				'Vendita Retail',
				'Vendita Commercianti',
				'Economica',
				'Tutto il Resto'
			],
			'gyu_tags' => [
				'Front End',
				'Back End',
				'Database',
				'Server',
				'Experience'
			]
		];

		$scrum_object = LoadClass('scrums', 1)->create($scrum);

		return $this->response($scrum_object);

	}

}