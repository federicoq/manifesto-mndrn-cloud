<?php

class scrums_stories extends application_object {

	var $gyu_table = 'scrums_stories';
	var $gyu_id = 'scrums_story_id';

	function body($args) {

		$body = LoadClass('scrums/stories/bodies', 1);
		$body->setAttr('scrums_story_id', $this->scrums_story_id);
		$body->setAttr('title', $args['title']);
		$body->setAttr('body', $args['description']);
		$body->setAttr('user_id', $args['user_id']);
		$body->setAttr('estimate', $args['estimate']);

		if($body->hangExecute()) {
			return $body;
		}

		return false;

	}

	function ContextBody() {

		$this->meta_add('body', LoadClass('scrums/stories/bodies', 1)->filter(['scrums_story_id', $this->scrums_story_id], ['active', 1], 'ONE'));

	}

	function deleteExecute() {

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());
		return parent::putExecute();

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());
		return parent::hangExecute();

	}

}