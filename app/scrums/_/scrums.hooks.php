<?php

// Update Stories - Estimation. (right now)
\Gyu\Hooks::set('scrums.stories.bodies.approved', function($story_id) {

	$story = LoadClass('scrums/stories', 1)->get($story_id, ['Body']);

	if($story) {
		$story->setAttr('estimate', $story->meta_get('body')->estimate);
		$story->putExecute();
	}

});