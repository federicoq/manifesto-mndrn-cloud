<?php

class scrums_roles extends application_object {

	var $gyu_table = 'scrums_roles';
	var $gyu_id = 'scrums_role_id';

	# admin | scrum master | 

	function deleteExecute() {

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());
		return parent::putExecute();

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());
		return parent::hangExecute();

	}

}