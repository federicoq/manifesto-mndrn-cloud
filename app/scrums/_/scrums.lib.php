<?php

class scrums extends application_object {

	var $gyu_table = 'scrums';
	var $gyu_id = 'scrum_id';

	var $gyu_meta = [ '@version' => 1 ];

	function create($args) {

		$this->refill($args);
		if($this->hangExecute()) {

			// Roles!
			if($this->gyu_roles) {
				foreach($this->gyu_roles as $role)
					$this->role_create($role, $color = false);
			}

			// Statuses
			if($this->gyu_statuses) {
				foreach($this->gyu_statuses as $status) {
					$this->status_create($status, $position = false);
				}
			}

			// Actors
			if($this->gyu_actors) {
				foreach($this->gyu_actors as $actor) {
					$this->actor_create($actor, $color = false);
				}
			}

			// Epics
			if($this->gyu_epics) {
				foreach($this->gyu_epics as $epic) {
					$this->epic_create($epic, $color = false);
				}
			}

			// Tags
			if($this->gyu_tags) {
				foreach($this->gyu_tags as $tag) {
					$this->tag_create($tag, $color = false);
				}
			}

		}

		return $this;

		print_r($args);

	}

	/* Stories */

	function story_create($actor, $story_type, $user_id) {

		$story = LoadClass('scrums/stories', 1);

		$story->code = 'US-' . ((float)(FetchObject(Database()->query("SELECT COUNT(*) as A FROM scrums_stories WHERE scrum_id = " . $this->scrum_id))->A)+1);
		$story->scrum_id = $this->scrum_id;
		$story->scrums_actor_id = $this->actor_create($actor)->scrums_actor_id;

		if($story->hangExecute()) {
			return $story;
		}

		return false;

	}

	/* Statuses */

	function status_create($name, $position) {
		
		$already = LoadClass('scrums/statuses', 1)->filter(['scrum_id', $this->scrum_id], ['name', $name], 'ONE');
		if(!$already) {

			$new = LoadClass('scrums/statuses', 1);
			$new->setAttr('scrum_id', $this->scrum_id);
			$new->setAttr('name', $name);
			$new->setAttr('position', $position);
			$new->hangExecute();

			return $new;
		}

		return $already;

	}

	/* Epics */

	function epic_create($name, $color) {
		
		$already = LoadClass('scrums/epics', 1)->filter(['scrum_id', $this->scrum_id], ['name', $name], 'ONE');
		if(!$already) {

			$new = LoadClass('scrums/epics', 1);
			$new->setAttr('scrum_id', $this->scrum_id);
			$new->setAttr('name', $name);
			$new->setAttr('color', $color);
			$new->hangExecute();

			return $new;
		}

		return $already;

	}

	/* Roles */

	function role_create($name, $color) {

		$already = LoadClass('scrums/roles', 1)->filter(['scrum_id', $this->scrum_id], ['name', $name], 'ONE');
		if(!$already) {

			$new = LoadClass('scrums/roles', 1);
			$new->setAttr('scrum_id', $this->scrum_id);
			$new->setAttr('name', $name);
			$new->setAttr('color', $color);
			$new->hangExecute();

			return $new;
		}

		return $already;

	}

	/* Actors */

	function actors_array() {

		//$this->Context('Actors');

		$actors = [];
		foreach($this->meta_get('actors') as $actor)
			$actors[] = $actor->name;

		return $actors;

	}

	function actor_create($name, $color) {

		$already = LoadClass('scrums/actors', 1)->filter(['scrum_id', $this->scrum_id], ['name', $name], 'ONE');
		if(!$already) {

			$new = LoadClass('scrums/actors', 1);
			$new->setAttr('scrum_id', $this->scrum_id);
			$new->setAttr('name', $name);
			$new->setAttr('color', $color);
			$new->hangExecute();

			return $new;
		}

		return $already;

	}

	/* Tags */

	function tag_create($name, $color) {

		$already = LoadClass('scrums/tags', 1)->filter(['scrum_id', $this->scrum_id], ['name', $name], 'ONE');
		if(!$already) {

			$new = LoadClass('scrums/tags', 1);
			$new->setAttr('scrum_id', $this->scrum_id);
			$new->setAttr('name', $name);
			$new->setAttr('color', $color);
			$new->hangExecute();

			return $new;
		}

		return $already;

	}

	/* Contexts */
	function ContextActors() {
		$this->meta_add('actors', LoadClass('scrums/actors', 1)->filter(['scrum_id', $this->scrum_id]));
	}

	function ContextStatuses() {
		$this->meta_add('statuses', LoadClass('scrums/statuses', 1)->filter(['scrum_id', $this->scrum_id]));
	}

	function ContextRoles() {
		$this->meta_add('roles', LoadClass('scrums/roles', 1)->filter(['scrum_id', $this->scrum_id]));
	}

	function ContextStories() {
		$this->meta_add('stories', $this->ForContext(LoadClass('scrums/stories', 1)->filter(['scrum_id', $this->scrum_id], ['LIMIT', 0, 10000000]), 'Body'));
	}

	/* Overrides of the default methods */
	function deleteExecute() {

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());
		return parent::putExecute();

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());
		return parent::hangExecute();

	}

}