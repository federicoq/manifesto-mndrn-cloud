<?php

class scrums_stories_bodies extends application_object {

	var $gyu_table = 'scrums_stories_bodies';
	var $gyu_id = 'scrums_stories_body_id';

	function approve() {

		$this->setAttr('active',1);
		$this->setAttr('approve_time', time());

		Database()->query("UPDATE scrums_stories_bodies SET active = 0 WHERE scrums_stories_body_id = " . $this->scrums_story_id);

		$r = $this->putExecute();

		if($r) {
			\Gyu\Hooks::get('scrums.stories.bodies.approved', $this->scrums_story_id);
			return $r;
		}

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());
		return parent::hangExecute();

	}

}