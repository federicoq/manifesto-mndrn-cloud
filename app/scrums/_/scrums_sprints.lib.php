<?php

class scrums_sprints extends application_object {

	var $gyu_table = 'scrums_sprints';
	var $gyu_id = 'scrums_sprint_id';

	function deleteExecute() {

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());
		return parent::putExecute();

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());
		return parent::hangExecute();

	}

}