<?php

class scrums_statuses extends application_object {

	var $gyu_table = 'scrums_statuses';
	var $gyu_id = 'scrums_status_id';

	function deleteExecute() {

		$this->setAttr('delete_time', time());
		return $this->putExecute();

	}

	function putExecute() {

		$this->setAttr('update_time', time());
		return parent::putExecute();

	}

	function hangExecute() {

		$this->setAttr('creation_time', time());
		return parent::hangExecute();

	}

}