<?php

class application_controller extends standardController {

	/**
	 * Results:
	 *
	 * This is the main stream output.
	 * the $this->response() method call it.
	 *
	 */
	public $result = [];
	
	/**
	 * Directives:
	 *
	 * Directives are rules that can be crossed by ACL or other injected information..
	 * directives are processed globally, by api/index but they can be used also in controllers methods.
	 *
	 * globally talking:
	 *
	 *	- redirect	path		call another api at the end.
	 *	- logout	true|false	destroy the session.
	 *
	 * NOT YET IMPLEMENTED
	 */
	public $directives = [];

	public function error($what, $error) {
		http_response_code(400);
		return [
			'@status' => 'KO',
			'error' => $what,
			'error_no' => $error ? $error : 1
		];
	}

	public function _data($content, $field = 'data') {

		$this->result[$field] = $content;
		return $this;

	}

	public function _r($code = 'OK') {

		$this->result['@status'] = $code;

		return $this->result;

	}

	public function response($content = false) {

		if($content) {
			if(count($this->result['data']) == 0)
				$this->result['data'] = $content;
		}

		if(!isset($this->result['data'])) {
			return [
				'@status' => 'OK',
				'data' => $this->result
			];
		} else
			return $this->result;
		
	}

	/**
	 * Response, with grid info for VUE Component.
	 *
	 */
	public function responseGrid($content = false, $spec = false, $args) {
		
		$content = $this->response($content);
		$content['grid'] = !isset($args['fields']) ? $this->grid_fields : $this->{$args['fields']};
		if($spec == false) {
			$content['sorting'] = ['key' => '', 'dir' => 'ASC'];
			$content['pagination'] = ['current' => 1, 'tot' => 1];
		} else {
			unset($spec['results']);
			foreach($spec as $k => $v) {
				$content[$k] = $v;
			}
		}

		return $content;

	}

	/**
	 * Build Where
	 *
	 * Utility class that helps to generate the SQL Query from the "GenericGrid" of Vue-Frontend.
	 * @param array	$args	This come from Vue-Frontend ux.query ['k' => ['q' => 'value', 'operator' => '=']]
	 * @return string
	 */
	public function buildWhere($args) {

		$pattern = '/^(([<>]|[=])|(!=))/';

		foreach($args as $k => $v) {

			$where_partial = [];
			#$blocks = explode(" | ", $v['q']);

			$matched = preg_split('/[&\|]/', $v['q']);
			#die();

			foreach($matched as $block) {

				$block = trim($block);

				// I need to understand if the $Q has a comparison char.
				if(preg_match($pattern, trim($block))) {
					$v['operator'] = 'FULLP';

					/* check for datas */
					$parts = preg_split($pattern, $block);
					foreach($parts as $singlePart) {
						if(strlen(trim($singlePart)) > 0) {

							$singlePart = trim($singlePart);

							if(strstr($singlePart, '/') && count(explode("/", $singlePart)) >= 2) {
								
								$portions = explode("/", $singlePart);
								if(count($portions) == 2) {
									$toRep = "'".date('Y-m-d', mktime(0,0,0,$portions[0],1,$portions[1]))."'";
								} else if(count($portions) == 3)
									$toRep = "'".date('Y-m-d', mktime(0,0,0,$portions[1],$portions[0], $portions[2]))."'";
								
								$jBlock = str_replace($singlePart, $toRep, $block);

							} else {
								if(!is_numeric($singlePart) && !is_numeric(trim(str_replace('=', '', $singlePart))))
									$jBlock = str_replace($singlePart, "'" . $singlePart . "'", $block);
								else
									$jBlock = str_replace($singlePart, $singlePart, $block);
							}

						}
					}

				} else {
					$jBlock = $block;
				}

				if($v['operator'] == 'LIKE') {
					$where_partial[$block] = "LOWER(`$k`) LIKE '%".strtolower(urldecode($jBlock))."%'";
				} else if($v['operator'] == 'FULLP') {
					$where_partial[$block] = "`$k` ".strtolower($jBlock);
				}
				else {
					$where_partial[$block] = "`$k` ".$v['operator']." " . (is_numeric($jBlock) ? $jBlock : '"'.$jBlock.'"');
				}

			}

			$tmp = str_replace(['&', '|'], ['AND', 'OR'], $v['q']);

			foreach($where_partial as $i => $j)
				$tmp = str_replace($i, $j, $tmp);

			$out[] = "(".$tmp.")";

		}

		return implode(' AND ', $out);
	
	}

	/**
	 * Check Rights
	 *
	 * Check if the ACL of the $user for the given $method for the given ($this) controller is enought to execute
	 * the method.
	 *
	 * @param 	string 	$method 	The desired method to be executed.
	 * @param 	users 	$user 		The user that try to execute the given Method
	 * @return 	false or array of directives.
	 */
	public function canExecute($method, $user) {

		if(isset($this->acl[$method])) {
			
			if(!is_object($user))
				return false;

			if(method_exists($user->gyu_group, $this->acl[$method])) {
				$this->directives = $user->gyu_group->{$this->acl[$method]}($user);
				return $this->directives;
			}
			else
				return false;

		} else {

			return true;
		
		}

	}

	/**
	 * ForContext (foreach)
	 *
	 * Cycle all $array and set the context.
	 *
	 * @param	array $array 		Items to be cycled.
	 * @param 	string $context 	Context to be setted.
	 * @param 	mixed $args 		Args passed to the Context Method.
	 * @return 	array
	 */
	public function ForContext($array, $context, $args = false) {
		foreach($array as $single) {
			if(is_object($single) && method_exists($single, 'setContext'))
				$single->setContext($context, $args);
		}
		return $array;
	}

	/**
	 * Crud Fields
	 *
	 * This method allow the front-end to popolate the starting-empty object.
	 *
	 * @uses this->crud_field as base object.
	 * @return	array 	Gyural Exchange Frontend
	 */
	function ApiCrudfields() {
		foreach($this->crud_fields as $k) {
			$out[$k] = '';
		}
		return $this->response($out);
	}

	/**
	 * Sample `Hello` method.
	 *
	 * @return	array 	Gyural Exchange Frontend
	 */
	function ApiHello() {
		$this->result['controller'] = get_class($this);
		return $this->response();
	}

}