<?php

class applicationCtrl extends application_controller {

	/**
	 * ApiMenu
	 *
	 * Obtain the Menu for the current logged user.
	 */
	function ApiMenu() {

		if(!Me())
			return $this->response();

		/*
		$revision = FetchObject(Database()->query("SELECT COUNT(*) as T FROM `_app_cars_revisions` WHERE approved_time = 0 AND delete_time = 0"))->T;
		
		$contracts = [
			'toValidate' => FetchObject(Database()->query("SELECT COUNT(*) as T FROM `_app_contracts` WHERE accepted = 0 AND isDraft = 0 AND delete_time = 0"))->T,
		];
		*/

		$menu = [
			'notifications' => FetchObject(Database()->query("SELECT COUNT(*) as tot FROM `_app_notifications` WHERE user_id = '".Me()->user_id."' AND read_time = 0"))->tot,
			'broadcast' => false,//['warning', 'Manutenzione in corso nell\'area contratti.'],
			'top' => [
				/*'/customers' => [
					'section' => 'customers',
					'name' => 'Contatti',
					'right' => 'is_admin'
				],*/
				'/users/list' => [
					'section' => 'users',
					'name' => 'Utenti',
					'right' => 'is_admin'
				],
				/*
				'/contracts' => [
					'section' => 'contracts',
					'name' => 'Contratti (' . $contracts['toValidate'] . ';' . $contracts['toConsolidate'] .';'.$contracts['expiring'].')',
					'right' => 'contract_admin'
				]
				*/
			],
			/*'setup' => [
				'sidebar' => 'SetupMenu',
				'menu' => [
					'/links/manage' => [ 'name' => 'Link Utili', 'right' => 'settings_view' ],
					'/users/list' => [ 'name' => 'Utenti', 'right' => 'settings_view' ],
					'/setup/params' => [ 'name' => 'Parametri', 'right' => 'is_superadmin' ],
				],
			],*/
			'customers' => [
				'sidebar' => 'CustomersMenu',
				'menu' => [
					'/dash' => [
						'name' => 'Capitolati',
						'right' => 'logged'
					],
					'/users/list' => [
						'name' => 'Utenti',
						'right' => 'is_admin'
					],
					'/hours' => [
						'name' => 'Gestione Ore',
						'right' => 'is_admin'
					],
					'/consuntivo' => [
						'name' => 'Consuntivi',
						'right' => 'is_admin'
					],
					'/customers/import' => [
						'name' => 'Import',
						'right' => 'lead_admin'
					],
				]
			],
			'customers/import' => [
				'sidebar' => 'CustomersMenu',
				'menu' => [
					'/dash' => [
						'name' => 'Capitolati',
						'right' => 'logged'
					],
					'/users/list' => [
						'name' => 'Utenti',
						'right' => 'is_admin'
					],
					'/hours' => [
						'name' => 'Gestione Ore',
						'right' => 'is_admin'
					],
					'/consuntivo' => [
						'name' => 'Cosuntivi',
						'right' => 'is_admin'
					],
					'/customers/import' => [
						'name' => 'Import',
						'right' => 'lead_admin'
					],
				]
			],
			'users' => [
				'sidebar' => 'UsersMenu',
				'menu' => [
					'/dash' => [
						'name' => 'Capitolati',
						'right' => 'logged'
					],
					'/users/list' => [
						'name' => 'Utenti',
						'right' => 'is_admin'
					],
					'/hours' => [
						'name' => 'Gestione Ore',
						'right' => 'is_admin'
					],
					'/consuntivo' => [
						'name' => 'Cosuntivi',
						'right' => 'is_admin'
					],
					'/customers/import' => [
						'name' => 'Import',
						'right' => 'lead_admin'
					],
				]
			],
			'profile' => [
				'sidebar' => 'UsersProfileMenu'
			],
			'notification' => [
				'sidebar' => 'NotificationsMenu'
			]
		];

		/*
		if(Me()->Right('can_it') && !Me()->Right('deals_me'))
			$menu['top']['/deals/requests'] = [
					'section' => 'deals',
					'name' => 'Deals' . ($deals > 0 ? ' ('.$deals.')' : ''),
					'right' => 'can_tickets'
				];
		*/
	
		return $this->response($menu);

	}

	/**
	 * Upload Assets
	 *
	 * Upload a file into the `upl` directory.
	 */
	function ApiUpload() {

		ini_set('memory_limit', -1);
		set_time_limit(0);

		$str = $_REQUEST['file'];

		$re = '/(data:(.+\/.+);base64,)/';

		preg_match_all($re, str_replace('data:;base64,', 'data:unknown/a;base64,', substr($str, 0, 200)), $matches);
		#var_dump($str);
		if(isset($matches[0][0])) {
			
			$real_file = str_replace($matches[0][0], '', $str);
			$real_file = base64_decode(str_replace(' ', '+', $real_file));

			$local_file_name = time() . '-' . str_replace(' ', '', $_REQUEST['file_name']);

			if(file_put_contents(upload . $local_file_name, $real_file)) {
				return $this->response(['url' => '/' . upl . '/' . $local_file_name]);
			}

		} return $this->error();

	}

	function ApiVersion() {
		return $this->response([
			'app' => '0.1',
			'url' => 'http://status.fdrcq.com/',
			//'url' => 'http://localhost/',
			'back' => '0.1'
		]);
	}

}