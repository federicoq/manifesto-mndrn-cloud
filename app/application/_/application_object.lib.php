<?php

LoadClass('calendars/traits/events');

class application_object extends standardObject {

	function get($id, $contexts) {

		$obj = parent::get($id);
		if($obj) {

			if(is_array($contexts)) {
				foreach($contexts as $single_context) {

					if(is_array($single_context)) {
						$single_context_name = $single_context[0];
						$single_context_args = $single_context[1];
						$obj->setContext($single_context_name, $single_context_args);
					} else
						$obj->setContext($single_context);
				}
			}

			return $obj;

		}

	}

	function meta_add($key, $value) {

		$this->gyu_meta[$key] = $value;

	}

	function meta_get($key) {

		if(!isset($this->gyu_meta[$key])) {
			$this->setContext(ucfirst($key));
		}

		return $this->gyu_meta[$key];

	}

	/**
	 * Set a $context for the given ($this) object.
	 *
	 * The context is the way that the model is manipolated to expose the data to the API Layer.
	 *
	 */
	function setContext($context, $args = false) {

		$context_method = 'Context' . ucfirst($context);

		if(method_exists($this, $context_method)) {
			$this->{$context_method}($args);
			return $this;
		}

		return $this;

	}

	/**
	 * Grid Helper
	 *
	 * Generate query and prepare content for GenericGrid Vue Component.
	 *
	 */
	function grid($args) {

		$query[] = 'FROM `'.$this->gyu_table.'`';

		if(isset($args['body'])) {
			$query[] = $args['body'];
		}

		if(isset($args['sorting'])) {
			$query[] = "ORDER BY " . $args['sorting']['key'] . ' ' . $args['sorting']['dir'];
		}

		if(isset($args['pagination'])) {
			$query_limit = " LIMIT ".(($args['pagination']['current']-1)*$args['pagination']['pp'])."," . $args['pagination']['pp'];
		}

		$count_query = "SELECT COUNT(*) AS tot " . implode(" ", $query);
		$select_query = "SELECT * " . implode(" ", $query) . $query_limit;

		$out['pagination'] = [
			'pp' => $args['pagination']['pp'],
			'current' => $args['pagination']['current'],
			'num_res' => FetchObject(Database()->query($count_query))->tot,
			'tot' => ceil(FetchObject(Database()->query($count_query))->tot / $args['pagination']['pp'])
		];

		$out['pagination']['show'] = $out['pagination']['tot'] > 1;

		$out['sorting'] = $args['sorting'];
		$out['results'] = FetchObject(Database()->query($select_query), 1, get_class($this));
		$out['queries'] = [$count_query, $select_query];

		if(isset($args['context'])) {
			foreach($out['results'] as $res) {
				if($res->setContext($args['context']));
			}
		}

		if($args['advanced']) {
			$out['advanced'] = 'ok';
		}

		return $out;

		return [
			'count_query' => $count_query,
			'select_query' => $select_query
		];

	}

	public function ForContext($array, $context, $args = false) {
		foreach($array as $single) {
			if(is_object($single) && method_exists($single, 'setContext'))
				$single->setContext($context, $args);
		}
		return $array;
	}
	
}