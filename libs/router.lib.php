<?php

/**
 * Gyural > Router
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class router {

	/**
	 * Array of routes
	 * @var array
	 */
	var $routes = [];

	/**
	 * Array of reverse routes
	 * @var array
	 */
	var $reverse = [];

	/**
	 * Rewrite an url to a determinate Controller/Page
	 * 
	 * @param  string $match Pattern to match
	 * @param  string $direction Rewrite rule
	 * @param  integer $code HttpCode Response
	 * @param  array $filters Filters to attach to the execution (gyuFilters @ standardController)
	 * @return false
	 */
	function map($match, $direction, $code = 200, $filters = null) {

		$_match = $match;
		$match = '/^'.str_replace(array('#', '/'), array('(.+)', "\/"), $match) . '/';
		$this->routes[$match] = array($direction, $code, $filters);
		@$this->reverse[$direction] = $_match;

	}

	/**
	 * Rewrite an url to a determinate Controller/Page
	 * 
	 * @param  string $match Pattern to match
	 * @param  string $direction Rewrite rule
	 * @param  integer $code HttpCode Response
	 * @param  array $filters Filters to attach to the execution (gyuFilters @ standardController)
	 * @return false
	 */
	function b_map($match, $direction, $code = 200, $filters = null) {

		$regex = '/(:|;)(\w+)/i';

		preg_match_all($regex, $match, $matches);

		if(is_array($matches[1])) {
			$methods = $matches[1];
			$varNames = $matches[2];
		}

		$vec_From[] = '/';
		$vec_Repl[] = '\/';

		foreach($varNames as $index => $v) {
			$toReplace = $methods[$index] . $v;
			$with = '(\S+)' . ($methods[$index] == ':' ? '' : '?');
			$vec_From[] = $toReplace;
			$vec_Repl[] = $with;
		}


		$match_converted = '/^'.str_replace($vec_From, $vec_Repl, $match).'/';

		$this->routes[$match_converted] = array($direction, $code, $vec_From, $filters);
		$this->reverse[$direction] = $match;

	}

	/**
	 * Check if a $page could be rewritten
	 * 
	 * @param  string $page
	 * @return mixed
	 */
	function _try($page) {

		foreach($this->routes as $pattern => $action) {

			if(preg_match($pattern, $page)) {

				preg_match_all($pattern, $page, $res);
				$out = $action[0];
				$code = $action[1];
				$filters = $action[2];
				
				if(count($action) == 4) {
					foreach($action[2] as $k => $v) {
						if($k > 0) {
							$rep[] = $v;
							$sos[] = str_replace(array(':', ';'), '', $v) . ':' . $res[$k][0];
						}
						$out = str_replace($rep, $sos, $out);
					}

				} else {
					unset($res[0]);
					foreach($res as $k)
						$out = preg_replace('/#/', $k[0], $out, 1);
				}
				
				return array('tmp' => $out, 'filters' => $filters);

			}

		}

		return $page;

	}

	/**
	 * Reverse a rewritted url to a standard one
	 * 
	 * @param  string $url
	 * @return string
	 */
	function _reverse($url) {

		if(isset($this->reverse[$url]))
			return '/' . ltrim($this->reverse[$url], '/');
		else {
			foreach($this->reverse as $search => $replace) {
				$regexp = '/^'.str_replace(array('#', '/'), array('(.+)', "\/"), $search) . '/';
				$regexp_ = '/^'.str_replace(array('#', '/'), array('(.+)', "\/"), $replace) . '/';

				$preg = preg_match($regexp, $url);

				if($preg) {
					preg_match_all($regexp, $url, $matches);
					// echo '<pre>';
					// print_r($matches);
					// echo '</pre>';
					$count = 0;
					$starting = str_replace('#', '#________________', $replace);
					$parts = explode('#', $starting);
					foreach($parts as $p => $v) {
						//echo $count;
						if(strstr($v, '________________')) {
							$parts[$p] = str_replace('________________', '#' . ($count), $v);
							$keyToReplace[] = '#' . $count;
							$valueToReplace[] = $matches[($count+1)][0];
							$count++;
						}
					}
					//print_r($valueToReplace);
					$newUrl = str_replace($keyToReplace, $valueToReplace, implode($parts));
					return '/' . $newUrl;
				}
			}
		}

	}

	/**
	 * Debug the active routes
	 * 
	 * @return array
	 */
	function debug() {

		echo '<pre>';
		print_r($this->routes);
		echo '</pre>';

		return $this->routes;

	}

	/**
	 * Retrive the $_REQUEST[$name] if $key is empty.
	 * 
	 * @param  string $name
	 * @param  mixed $key
	 * @return mixed
	 */
	static function input($name, $key) {

		if(empty($key))
			if(empty($_REQUEST[$name]))
				return false;
			else
				return $_REQUEST[$name];
		else
			return $key;

	}

}
