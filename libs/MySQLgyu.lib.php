<?php

/**
 * Gyural > MySQLgyu
 * This is an extension of MySQLi Standard Lib. <http://php.net/mysqli>
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class MySQLgyu extends mysqli {

	public function Query($query) {

		$qUID = uniqid();
		## Caution.. it won't work with UNIONS.
		if(stristr($query, "SELECT") && !stristr($query, "UNION")) {
			preg_match("/\s+from\s+`?([a-z\d_]+)`?/i", $query, $match);
			$tableName = $match[1];
		}

		if(!isset($GLOBALS["num_query"]))
			$GLOBALS["num_query"] = 0;

		$toLog = [];
		$toLog["query"] = $query;

		$GLOBALS["num_query"]++;
		$this->real_query($query);
		$a = new MySQLgyu_result($this);

		$stop = 0;

		if(dev)
			if(strlen($this->error) > 0) {
				$toLog["error"] = $this->error;
				$stop = 1;
			}

		$GLOBALS["queries_archive"][] = $toLog;

		if($stop == 1)
			deb_error($this->error, 1);

		isset($tableName) ? $a->{excludeFieldPrefix . 'table'} = str_replace(tablesPrefix, '', $tableName) : ''; # Retrive table name from the $query. note up.

		return($a);

	}

}

class MySQLgyu_result extends mysqli_result {

	public function oggettizza($object, $args, $query) {

		if($args)
			$a = $this->fetch_object($object, $args);
		else
			$a = $this->fetch_object($object);

		if($a) {

			#isset($this->gyu_table) ? $a->{excludeFieldPrefix . 'table'} = $this->{excludeFieldPrefix . 'table'} : ''; # as above.

			if(method_exists($a, 'gyu_tablesPrefix'))
				$a->gyu_tablesPrefix();

			if(dev) $a->gyu_query_performed = $query;

			return($a);
		}

	}

}
