<?php

/**
 * Gyural > Funcs > Gyural
 * Kernel File
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

/**
 * Autoload .php files stored into /funcs/autoload
 * 
 * @return false
 */
function gyu_autoload() {
	$path = absolute . 'funcs/autoload/';
	if ($handle = opendir($path)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry[0] != "." && is_file($path . $entry)) { // + check on .php format.
				if(strstr($entry, '.php'))
					include_once $path . $entry;
			}
		}
		closedir($handle);
	}
}

/**
 * Support for PSR4 Autoload
 * http://www.php-fig.org/psr/psr-4/
 * 
 * @return false
 */
function gyu_psr4() {
	set_include_path(absolute.'vendors/');
	spl_autoload_extensions('.php');
	spl_autoload_register('gyuAutoloader');
}

/**
 * Implementation of PSR4 + Underscore App
 * 
 * @param  string $class Name of class to load
 * @return [type]
 */
function gyuAutoloader($class) {

	#$partial = explode('\\', $class);
	#$singleName = $partial[count($partial)-1];

	$createVar = function($string) {

		$parts = explode('_', $string);
		$counter = 0;
		$staticParts[] = $parts[0] . '_';
		$staticParts[] = $parts[0] . '/';
		foreach($parts as $i => $part) {
			if($i == 0) continue;
			foreach($staticParts as $k=>$parte) {
				$staticParts[] = $parte . $part . '_';
				$staticParts[] = $parte . $part . '/';
			}
		}

		foreach($staticParts as $part) {
			$part = rtrim(rtrim($part, '/'), '/');
			if(strlen($part) == strlen($string))
				$test[] = $part;
		}
		
		return $test;
		
	};

	$tests = $createVar($class);

	// Check if the $class is a 'gyural standard lib/underscore lib'
	if(!stristr($class, 'ctrl')) {
		foreach($tests as $testClass) {
			$test = LoadClass($testClass, false, null, true);
			if($test == $class)
				return true;
		}
	} else {
		foreach($tests as $testClass) {
			$test = LoadApp($testClass);
			if($test == $class)
				return true;
		}
	}

	$path = absolute.'vendors/'.str_replace("\\", "/", $class).'.php';

	if(is_file($path))
		include_once $path;
	else {
		$path = absolute.'vendors/Gyu/'.str_replace("\\", "/", $class).'.php';
		#echo $path;
		if(is_file($path))
			include_once $path;
		// deb_error($class . ' not found in vendors/libs/underscore.', 1);
	}
}

/**
 * Routing the $page request
 * 
 * @param string $page
 * @param boolean $app
 * @param object $router
 */
function Route(&$page, $app = false, $router = false) {
	
	if(!$router)
		global $router;

	$GLOBALS["app"] = false;
	$goSafe = true;
	
	if($app)
		$GLOBALS["app"] = true;
	
	// Check if something is manually called.
	if($page != NULL)
		$route = $page;
	else {
		$route = trim($_SERVER["PATH_INFO"], '/');
	}
	
	$environment = LoadClass('gyural', 1, MethodDetect());
	
	// The IndexApp is called when nothing it's requested.
	if(strlen($route) == 0)
		$route = IndexApp;
	if(isset($router)) {
		$tmp = $router->_try($route);
		if(is_array($tmp)) {
			$filters = $tmp["filters"];
			$tmp = $tmp["tmp"];
		}
	}

	$internalExecution = false;
	if(!is_string($tmp)) {
		$internalExecution = true;
	} else {
		$route = $tmp;
	}
	
	// Splitting $route by slash
	$parts = explode("/", $route);
	
	// Searching for /key:value/ remove it if found, then re-implode the "ipotetical app"
	foreach($parts as $part) {
		if(!strstr($part,varDivider)) {
			$file[] = $part;
			if(strlen($part) > 0)
				if($part[0] == '_')
					$goSafe = false;
		}
		else {
			@list($key, $value) = explode(varDivider, $part);
			$_GET[$key] = $value;
			$_REQUEST[$key] = $value;
		}
	}
	
	if(!$internalExecution) {
		if(!$file)
			$file = explode('/', IndexApp);
		$page = implode("/", $file);
	} else {
		$page = $tmp;
		$environment->internal_execution = true;
	}

	$environment->candidate = $page;

	#print_R($environment);

	// Check added into v1.4 #
	if($app == false && $goSafe == false)
		header('Location: /' . IndexApp);
	
	if($internalExecution)
		return $environment;
	// Else, go on with the standard checks.

	// Guess the controllers trials
	$rounds = sizeof($file);
	for($index = 0; $index < $rounds; $index++) {
		$subCtrls[] = implode('_', $file);
		unset($file[sizeof($file)-1]);
	}

	// SISTEMAZIONE 9/6/16 => DOPPIA ENTRATA NEL COSTRUTTORE DEL CONTROLLER.
	/*
	foreach($environment->methods as $type => $metodo_check) {
		if($metodo_check)
			$prefix_methods_to_check[] = $type;
	}
	$prefix_methods_to_check[] = 'ctrl';


	foreach($subCtrls as $r => $ctrl) {

		if(LoadController($ctrl)) {

			$pars_safe = $parts;

			for($i = $r; $i >= 0; $i--)
				unset($pars_safe[$i]);

			if(empty($pars_safe))
				$pars_safe[] = 'index';

			$metodo_da_controllare_esistenza = implode('__', $pars_safe);
			
			$metodi_da_provare[] = implode('__', $pars_safe);
			if(!in_array('index', $metodi_da_provare)) // Se già presente, o completamente assente lo ignoro altrimenti, in ogni caso deve sempre essere controllato anche index!
				$metodi_da_provare[] = 'index';

			foreach($metodi_da_provare as $method) {

				foreach($prefix_methods_to_check as $method_type) {

					if(method_exists($ctrl . 'Ctrl', $method_type . $method)) {
						$environment->availableControllers = $ctrl . 'Ctrl/' .  $method_type . $method;
						break;
					}
				}

			}

			if(!$environment->availableControllers) {
				if(method_exists($ctrl . 'Ctrl', $type . $metodo_da_controllare_esistenza)) {
					$environment->availableControllers = $ctrl . 'Ctrl/' .  $type . $metodo_da_controllare_esistenza;
					break;
				}	
			}

		} else {
			if(function_exists('deb_log'))
				deb_log("Controller " . $ctrl . " not found");
		}
	}

	// @ \\
	*/

	if(!$environment->availableControllers) {
		foreach($subCtrls as $ctrls) {
			
			$environment->application = $ctrls;

			if($environment->controllerExists($environment->application)) {

				$environment->availableControllers = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest($environment->candidate . '/index'), $environment->methods);

				if(!$environment->availableControllers) {
					if($environment->Controller($environment->application)->index_tollerant == false)
						$environment->availableControllersError = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest(ErrorApp), $environment->methods);
					else {
						if(!$environment->availableControllers = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest('index'), $environment->methods))
							$environment->availableControllersError = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest(ErrorApp), $environment->methods);
					}
				} else
					break;
			}
		}

	}


	if(!$environment->availableApplication = IsApplicationCallable($page)) {
		if(!$environment->availableApplicationError = IsApplicationCallable($environment->application . '/' . ErrorApp))
			$environment->availableApplicationError = IsApplicationCallable(ErrorApp);
	}

	$GLOBALS["__route_stack"][] = $environment;
	if(isset($filters))
		$environment->gyu_filters = $filters;

	return $environment;
	
}

/**
 * Check if isGyural
 * 
 * @return false
 */
function isGyu() {
	!defined('gyu') ? die("Error") : '';
}

/**
 * Error Trigger
 * 
 * @param  mixed $e
 * @return false
 */
function __error($e) {
	echo '<p>'.$e->getMessage().'</p>';
}
