<?php

/**
 * Gyural > Funcs > Funcs
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

# Require a page of functions. Force the ~funcs/repository.php prefixing * to the $repository
function Functions($repository, $forceGlobal = 0) {

	if(strstr($repository, '/')) {
		$parts = explode('/', $repository);
		$repository = $parts[0];

		$repFile = implode('_', $parts);

	} else
		$repFile = $repository;

	$baseDir = application . $repository . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR;
	$baseDirT = applicationCore . $repository . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR;
	
	if(legacy == 0)
		$baseTTDir = applicationCore . $repository . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR;

	$baseTDir =  absolute . 'funcs'.DIRECTORY_SEPARATOR.'third'.DIRECTORY_SEPARATOR;
	
	$priority["specific"][] = $baseDir . $repFile . '.funcs.php';
	$priority["specific"][] = $baseDir . 'funcs.php';
	$priority["specific"][] = $baseDir . 'func.php';

	$priority["specific"][] = $baseDirT . $repFile . '.funcs.php';
	$priority["specific"][] = $baseDirT . 'funcs.php';
	$priority["specific"][] = $baseDirT . 'func.php';
	
	if(legacy == 0) {
		$priority["specific"][] = $baseTTDir . $repFile . '.funcs.php';
		$priority["specific"][] = $baseTTDir . 'funcs.php';
		$priority["specific"][] = $baseTTDir . 'func.php';
	}

	$priority["global"][] = $baseTDir . $repFile . '.funcs.php';
	$priority["global"][] = $baseTDir . $repFile . '.php';
	
	if($forceGlobal == 1)
		unset($priority["specific"]);
	
	foreach($priority as $list) {
		if(!@$wf)
			foreach($list as $file)
				if(is_file($file))
					$wf=$file;
	}
	
	$file = $wf;
	include_once $wf;
	
}

# Call a Specific Function
function CallFunction($repository, $func) {

	deb_log($repository . '.' . $func, 'funcs');

	$forceGlobal = 0;

	if($repository[0] == '*') {
		$forceGlobal = 1;
		$repository = substr($repository,1);
	}

	Functions($repository, $forceGlobal);
	$args = func_get_args();
	
	if(count($args) < 2) { 
		deb_error('Error. Impossible to determine the function repository');
		return false;
	} else {
		unset($args[0], $args[1]);
		return(call_user_func_array(str_replace('/', '_', $repository) . "__" . $func, $args));
	}

}
