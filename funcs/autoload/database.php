<?php

/**
 * Gyural > Funcs > Database
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

/**
 * Parse a string ($path) as $pattern to determine the connection credentials
 * 
 * @param string $path
 * @param string $pattern
 */
function ParseDatabase($path, $pattern) {
	preg_match($pattern, $path, $results);
	if($results[1] == "mysql") {
		try {
			$a =  MysqlConnect($results[4], $results[2], $results[3], $results[5]);
			$a->set_charset("utf8");
			return $a;
		} catch (Exception $e) { __error($e); }
	} else {
		throw new Exception('Database engine not supported');
	}
}

/**
 * MySQL connection
 * 
 * @param string $server
 * @param string $username
 * @param string $password
 * @param string $database
 * @return MySQLgyu
 */
function MysqlConnect($server, $username, $password, $database) {
	$c = new MySQLgyu($server, $username, $password, $database);
	if($c->connect_error) {
		deb_error('Database: ' . $c->connect_error);
	}
	else return $c;
}

/**
 * Parse a Resource as standardObject (or more specific $classe)
 * 
 * @param resource $oggetto
 * @param integer $alwayArray
 * @param string $classe
 * @param array $args
 * @param string $query
 * @return mixed
 */
function FetchObject($oggetto, $alwayArray = 0, $classe = 'standardObject', $args = null, $query) {
	if(!is_numeric($alwayArray))
		$classe = $alwayArray;
	$classe = !class_exists($classe) ? LoadClass($classe) : $classe;
	$numeroRisultati = $oggetto->num_rows;
	if($numeroRisultati == 1 && $alwayArray == 0)
		return $oggetto->oggettizza($classe, $args, $query);
	else if($numeroRisultati > 1 || ( $numeroRisultati > 0 && $alwayArray == 1)) {
		while($r = $oggetto->oggettizza($classe, $args, $query))
			$out[] = $r;
		$out[0]->{excludeFieldPrefix . 'first'} = true;
		$out[count($out)-1]->{excludeFieldPrefix . 'last'} = true;
		return $out;
	} else
		return false;
}

/**
 * Helper to create a MySQL query
 * INSERT/UPDATE
 * 
 * @param string $tipo
 * @param string $tabella
 * @param array $campi
 * @param mixed $identified
 */
function CreateQuery($tipo, $tabella, $campi, $identifier) {

	$prevent = preventDuplicate;

	if($tipo == "0" || $tipo == "INSERT" || $tipo == "I") 
		$type = "INSERT"; 
	else if($tipo == 'SI') {
		$type = "INSERT"; 
		$prevent = false;
	}
	else 
		$type = "UPDATE";

	$porzioni = array();
	if($type == "UPDATE") {
		foreach($campi as $chiave => $valore) {
			if(!strstr($chiave, excludeFieldPrefix))
				$porzioni[] = '`'.$chiave.'` = ' . (!is_int($valore) ? "'".addslashes($valore)."'" : addslashes($valore));
		}
		return 'UPDATE `'.$tabella.'` SET ' . implode(", ", $porzioni) . " WHERE " . $identifier;
	} else if($type == "INSERT") {
		foreach($campi as $chiave => $valore) {
			if(!strstr($chiave, excludeFieldPrefix))
				$porzioni[] = '`'.$chiave.'` = ' . (!is_int($valore) ? "'".addslashes($valore)."'" : addslashes($valore));
		}
		if($prevent == true) {
			global $databaseDriver;
			if(!is_numeric(FetchObject($databaseDriver->query("SELECT * FROM `".$tabella."` WHERE " . implode(" AND ", $porzioni) . " LIMIT 1"))->id))
				$proceed = true;
			else
				$proceed = false;
		} else
			$proceed = true;
		if($proceed == true)
			return 'INSERT INTO `'.$tabella.'` SET ' . implode(", ", $porzioni);
		else
			return false;
	}
}

/**
 * Current active db session
 * 
 * @return MySQLgyu
 */
function Database() {
	global $databaseDriver;
	return $databaseDriver;
}
